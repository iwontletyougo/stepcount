package cn.tianyue0571.brunning;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import cn.tianyue0571.brunning.base.AbsBaseActivity;
import cn.tianyue0571.brunning.inject.FindView;

public class WebViewActivity extends AbsBaseActivity {

    private String url;

    @FindView(R.id.webview)
    WebView mWebView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_web_view;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        url = getIntent().getStringExtra("url");
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);//如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);//解决字符串换行问题
        webSettings.setUseWideViewPort(true); // 设置此属性，可任意比例缩放。
        webSettings.setBuiltInZoomControls(true);  // 设置是否可缩放
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setSupportZoom(true);  //缩放开关
        webSettings.setDefaultFontSize(14);
        webSettings.setDefaultTextEncodingName("UTF-8");

        mWebView.setWebViewClient(new WebViewClient() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.loadUrl(String.valueOf(request.getUrl()));
                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //重写此方法表明点击网页在当前的webview跳转,不跳到浏览器中
                view.loadUrl(url);
                return true;
            }

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //接受证书
                handler.proceed();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }


        });
        //查看页面
        mWebView.loadUrl(url);
    }
}
