package cn.tianyue0571.brunning.stepcount;

/**
 * 步数更新回调
 * Created by dylan on 16/9/27.
 */
public interface UpdateUiCallBack {
    /**
     * 更新UI步数
     *
     * @param stepCount 步数
     */
    void updateUi(int stepCount);
}
