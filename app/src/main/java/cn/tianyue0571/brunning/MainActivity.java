package cn.tianyue0571.brunning;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import cn.tianyue0571.brunning.base.AbsBaseActivity;
import cn.tianyue0571.brunning.inject.FindView;
import cn.tianyue0571.brunning.inject.OnClick;

public class MainActivity extends AbsBaseActivity {
    @FindView(R.id.tab_home)
    RadioButton tab_home;
    @FindView(R.id.tab_run)
    RadioButton tab_run;
    @FindView(R.id.tab_circle)
    RadioButton tab_circle;
    @FindView(R.id.tab_mine)
    RadioButton tab_mine;
    private List<Fragment> mFragmentList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        //        Intent intent = new Intent(this, WebViewActivity.class);
//        intent.putExtra("url", "file:///android_asset/bp/html/login.html");
////        intent.putExtra("url", "https://www.sina.cn");
////        intent.putExtra("url", "http://192.168.1.26/bp/html/login.html");
//        startActivity(intent);
//        begin();
        initFragments();
        loadFragment(0);
        setTabSelected(0);
    }

    private void loadFragment(int poi) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (mFragmentList == null) {
            initFragments();
        }
        if (!mFragmentList.get(poi).isAdded()) {
            transaction.add(R.id.fragment_container, mFragmentList.get(poi));
        }
        hidefragment(transaction, mFragmentList);
        transaction.show(mFragmentList.get(poi));
        transaction.commitNowAllowingStateLoss();
    }

    private void hidefragment(FragmentTransaction transaction, List<Fragment> fragmentList) {
        for (Fragment f : fragmentList
                ) {
            if (!f.isHidden())
                transaction.hide(f);
        }
    }

    private void initFragments() {
        if (null == mFragmentList) {
            mFragmentList = new ArrayList<>();
        }
        mFragmentList.add(TabFragment.newInstance("home"));
        mFragmentList.add(TabFragment.newInstance("run"));
        mFragmentList.add(TabFragment.newInstance("circle"));
        mFragmentList.add(TabFragment.newInstance("mine"));
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.tab_home, R.id.tab_run, R.id.tab_circle, R.id.tab_mine})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.tab_home:
                setTabSelected(0);
                loadFragment(0);
                break;
            case R.id.tab_run:
                setTabSelected(1);
                loadFragment(1);
                break;
            case R.id.tab_circle:
                setTabSelected(2);
                loadFragment(2);
                break;
            case R.id.tab_mine:
                setTabSelected(3);
                loadFragment(3);
                break;
        }
    }

    private void setTabSelected(int i) {
        resetColor(tab_home, tab_run, tab_circle, tab_mine);
        switch (i) {
            case 0:
                tab_home.setChecked(true);
                tab_home.setTextColor(getResources().getColor(R.color.yellow));
                break;
            case 1:
                tab_run.setChecked(true);
                tab_run.setTextColor(getResources().getColor(R.color.yellow));
                break;
            case 2:
                tab_circle.setChecked(true);
                tab_circle.setTextColor(getResources().getColor(R.color.yellow));
                break;
            case 3:
                tab_mine.setChecked(true);
                tab_mine.setTextColor(getResources().getColor(R.color.yellow));
                break;
        }
    }

    private void resetColor(RadioButton... radioButtons) {
        for (RadioButton radioButton :
                radioButtons) {
            radioButton.setChecked(false);
            radioButton.setTextColor(getResources().getColor(R.color.gray));
        }
    }
}
