package cn.tianyue0571.brunning;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;

import cn.tianyue0571.brunning.base.AbsBaseFragment;
import cn.tianyue0571.brunning.inject.FindView;
import cn.tianyue0571.brunning.inject.Init;
import cn.tianyue0571.brunning.inject.OnClick;
import cn.tianyue0571.brunning.inject.ViewUtils;

/**
 * $desc
 * 作者：10622 on 2018/11/14 15:11
 * 邮箱：1062258956@qq.com
 */
public class TabFragment extends AbsBaseFragment {
    private String url;

    @FindView(R.id.container)
    FrameLayout mFrameLayout;

    public static TabFragment newInstance(String tag) {
        Bundle args = new Bundle();
        args.putString("tag", tag);
        TabFragment fragment = new TabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int layoutRes() {
        return R.layout.fragment_tab;
    }

    @Override
    protected void onViewReallyCreated(View rootView) {

    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        String args = (String) getArguments().get("tag");
        switch (args) {
            case "home":
                url = "http://192.168.1.26/bp/html/index.html";
                new SetWebView().invoke();
                break;
            case "run":
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_run, null);
                ViewUtils.inject(view, new runAct());
                mFrameLayout.addView(view);
                break;
            case "circle":
                url = "http://192.168.1.26/bp/html/circle.html";
                new SetWebView().invoke();
                break;
            case "mine":
                url = "http://192.168.1.26/bp/html/my.html";
                new SetWebView().invoke();
                break;
        }

    }

    private class SetWebView {
        public void invoke() {
            WebView mWebView = new WebView(getActivity());
            mFrameLayout.addView(mWebView);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);//如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
            webSettings.setAllowFileAccess(true); //设置可以访问文件
            webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);//解决字符串换行问题
            webSettings.setUseWideViewPort(true); // 设置此属性，可任意比例缩放。
            webSettings.setBuiltInZoomControls(true);  // 设置是否可缩放
            webSettings.setLoadWithOverviewMode(true);
            webSettings.setSupportZoom(true);  //缩放开关
            webSettings.setDefaultFontSize(14);
            webSettings.setDefaultTextEncodingName("UTF-8");
            mWebView.setWebViewClient(new WebViewClient() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    view.loadUrl(String.valueOf(request.getUrl()));
                    return true;
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    //重写此方法表明点击网页在当前的webview跳转,不跳到浏览器中
                    view.loadUrl(url);
                    return true;
                }

                public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                    //接受证书
                    handler.proceed();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                }


            });
            //查看页面
            mWebView.loadUrl(url);
        }
    }

    class runAct {
        @FindView(R.id.tv_title)
        TextView mTitle;

        @OnClick({R.id.m_btn_run})
        public void onViewClick(View v) {
            switch (v.getId()) {
                case R.id.m_btn_run:
                    startActivity(new Intent(getActivity(), RunningActivity.class));
                    break;
            }
        }

        @Init
        public void actionInit() {
            mTitle.setText("跑步");
        }
    }
}
