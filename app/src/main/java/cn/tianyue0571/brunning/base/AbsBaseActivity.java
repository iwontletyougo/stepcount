package cn.tianyue0571.brunning.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import cn.tianyue0571.brunning.inject.ViewUtils;

/**
 * $desc
 * 作者：10622 on 2018/11/13 10:02
 * 邮箱：1062258956@qq.com
 */
public abstract class AbsBaseActivity extends AppCompatActivity {
    protected String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, getClass().getSimpleName());
        ActivityManager.getAppManager().addActivity(this);
        setContentView(getLayoutId());
        ViewUtils.inject(this);
        initData(savedInstanceState);
    }

    protected abstract int getLayoutId();

    protected abstract void initData(Bundle savedInstanceState);


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.getAppManager().finishActivity(this);
    }
}
