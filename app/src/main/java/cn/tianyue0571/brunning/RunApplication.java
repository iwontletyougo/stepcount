package cn.tianyue0571.brunning;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by jiahongfei on 2018/1/15.
 */

public class RunApplication extends Application {

    private static RunApplication sApplication;

    private int appCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        sApplication = this;

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                appCount++;
            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {
                appCount--;
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    /**
     * app是否在前台
     *
     * @return true前台，false后台
     */
    public boolean isForeground() {
        return appCount > 0;
    }

    public static RunApplication getApplication() {
        return sApplication;
    }

}
