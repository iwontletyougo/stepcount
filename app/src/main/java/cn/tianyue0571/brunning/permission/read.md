﻿动态申请权限,用法:example
private static final int PHOTO_CAMERA = 1000;
在需要申请权限的地方:
 //动态申请权限
PermissionHelper.requestPermisson(MainActivity.this, PHOTO_CAMERA, new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE});


在activity的onRequestPermissionsResult方法中:
@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.requestPermissionsResult(this, requestCode, permissions, grantResults);
    }

成功执行的方法:
	@PermissionSuccess(requestCode = PHOTO_CAMERA)
	private void todoSuccess(){
	//TODO
	} 


失败执行的方法:
	@PermissionFail(requestCode = PHOTO_CAMERA)
	private void todoFail(){
	//TODO
	} 
